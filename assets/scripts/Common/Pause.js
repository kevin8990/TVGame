const Global = require("Global");

cc.Class({
    extends: cc.Component,

    properties: {
    },

    onLoad: function () {
        Global.gameInfo.state = 'pause';
    },

    onContinueBtnClicked: function(event, cdata) {
        Global.gameInfo.state = 'running';
        this.node.parent.getChildByName('FingerCtrl').active = true;
        this.node.destroy();
    },

    onRestartBtnClicked: function(event, cdata) {
        Global.restData();
        this.node.destroy();
        cc.director.loadScene('Game');
    },

    onExitBtnClicked: function(event, cdata) {
        Global.restData();
        cc.director.loadScene('Main');
    },
});
