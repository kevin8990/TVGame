const UserData = require('UserData');

cc.Class({
    extends: cc.Component,

    properties: {
        rankNumber: 61000,
        onRankNumber: 6,
        selfScore:20,
        selfRank:cc.Node,
        selfName:cc.Label,
        rankList: {
            type:[cc.Node],
            default:[]
        },
    },

    onLoad: function () {
        this.initRank();
    },

    initRank: function() {
        this.selfScore = UserData.userInfo.score;
        var rankScore = [];
        rankScore.push(99987);
        rankScore.push(99941);
        rankScore.push(94721);
        rankScore.push(88745);
        rankScore.push(84187);
        rankScore.push(77987);

        for (var i = 0; i < this.rankNumber; i++) {
            rankScore.push(Math.ceil(Math.random() * 77986));
        }
        rankScore.push(this.selfScore);
        rankScore.sort((a, b) =>{return b - a});
     
        var selfRank = this.rankNumber + 1;
        for (var i = 0; i <= this.rankNumber; i++) {
            if (rankScore[i] == this.selfScore) {
                selfRank = i + 1;
                break;
            }
        }
        if (selfRank <= this.onRankNumber) {
            var name = this.rankList[selfRank - 1].getChildByName("Name").getComponent(cc.Label);
            var level = this.rankList[selfRank - 1].getChildByName("Level").getComponent(cc.Label);

            name.string = this.selfName.string;
            name.node.color = cc.Color.GREEN;
            level.string = this.selfScore;
            level.node.color = cc.Color.GREEN;
        }
        
        this.selfRank.getChildByName("Rank").getComponent(cc.Label).string = selfRank;
        this.selfRank.getChildByName("Level").getComponent(cc.Label).string = this.selfScore;
    },

    onCancelBtnClicked: function(event, cdata) {
        this.node.parent.getChildByName('FingerCtrl').active = true;
        this.node.destroy();
    },
});
