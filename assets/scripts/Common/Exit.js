cc.Class({
    extends: cc.Component,

    properties: {
    },

    onLoad: function () {
        
    },

    onConfirmBtnClicked: function(event, cdata) {
        cc.game.end();
    },

    onCancelBtnClicked: function(event, cdata) {
        this.node.parent.getChildByName('FingerCtrl').active = true;
        this.node.getChildByName('FingerCtrl').active = false;
        this.node.destroy();
    },
});
