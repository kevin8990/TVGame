
module.exports = {

    showTip: function (filename, parent) {
        cc.loader.loadRes(filename, cc.SpriteFrame, (err, spriteFrame) => {
            if (err) {
                cc.log("loadRes " + filename + 'error');
                return;
            }
            var node = new cc.Node();
            parent.addChild(node, cc.macro.MAX_ZINDEX);
            node.position = cc.v2(0, 0);
            var sprite = node.addComponent(cc.Sprite);
            sprite.spriteFrame = spriteFrame;

            var scaleBy = cc.scaleBy(0.2, 1.5, 1.5);
            var scaleTo = cc.scaleTo(0.2, 1, 1);
            var delay = cc.delayTime(0.5);
            var seq = cc.sequence(scaleBy, scaleTo, scaleBy, scaleTo, delay, cc.removeSelf());
            node.runAction(seq);
        });
    },

    //切换手指工具可用状态
    switchNodeFingerCtrl: function (node) {
        var fingerCtrl = node.getChildByName('FingerCtrl');
        fingerCtrl.active = !fingerCtrl.active;
    },

};