const userDataKey = 'userDataKey';

module.exports = {

    
    // 用户数据
    userInfo: {
        level: 1,    // 等级
        score: 0,    // 分数
        maxScore: 0, // 最高分
    },

    loadData() {
        let data = cc.sys.localStorage.getItem(userDataKey);
        if (data) {
            this.userInfo = JSON.parse(data);
        }
    },

    saveData() {
        cc.sys.localStorage.setItem(userDataKey, JSON.stringify(this.userInfo));
    },
    
};
