const Global = require('Global');
const UserData = require('UserData');

cc.Class({
    extends: cc.Component,

    properties: {
        score: {
            default: null,
            type: cc.Label
        },
        maxScore: {
            default: null,
            type: cc.Label
        }
    },

    onRestartBtnClicked: function(event, cdata) {
        Global.restData();
        this.node.destroy();
        cc.director.loadScene('Game');
    },

    onExitBtnClicked: function(event, cdata) {
        Global.restData();
        cc.director.loadScene('Main');
    },

    onLoad() {
        this.score.string = UserData.userInfo.score;
        this.maxScore.string = UserData.userInfo.maxScore;
    },
});