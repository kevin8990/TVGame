const Global = require('Global');
const Common = require('Common');

cc.Class({
    extends: cc.Component,

    properties: {

        // 游戏背景音乐
        audioBgm: {
            type: cc.AudioClip,
            default: null
        },

        //游戏失败音效
        audioFail: {
            type: cc.AudioClip,
            default: null
        },

        //引导界面
        gameGuide: {
            type: cc.Node,
            default: null
        },

        // 暂停页面
        pauseLayerPrefab: {
            default: null,
            type: cc.Prefab
        },

        // 结束遮罩
        gameOverPrefab: {
            default: null,
            type: cc.Prefab
        },
    },

    onLoad: function() {
        this.total = 0;
        cc.audioEngine.playMusic(this.audioBgm, true);
    },

    update: function(dt) {
        if (Global.gameInfo.state == 'running') {
            this.total += dt;
            if (this.total > 3) {
                this.gameOver();
            }
        }
    },

    // 开始游戏
    startGame: function() {
        this.gameGuide.destroy();
        Global.gameInfo.state = 'running';
    },

    // 游戏结束
    gameOver: function() {
        this.gameOverBox = cc.instantiate(this.gameOverPrefab);
        this.gameOverBox.parent = this.node;

        cc.audioEngine.stopMusic();

        cc.audioEngine.play(this.audioFail, false);

        Common.switchNodeFingerCtrl(this.node);
        Global.restData();
    },

    onKeyBackClicked: function () {
        if (Global.gameInfo.state == 'running') {
            Common.switchNodeFingerCtrl(this.node);
            let pauseLayer = cc.instantiate(this.pauseLayerPrefab);
            Common.switchNodeFingerCtrl(pauseLayer);
            this.node.addChild(pauseLayer);
        }
    },

    onEnterClicked: function () {
        if (Global.gameInfo.state == 'over') {
            this.startGame();
            return;
        };
    },

});