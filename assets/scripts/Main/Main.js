const Common = require('Common');
const UserData = require('UserData');

cc.Class({
    extends: cc.Component,

    properties: {
        // bgm
        audioBgm: {
            type: cc.AudioClip,
            default: null
        },
        exitLayerPrefab: {
            default: null,
            type: cc.Prefab
        },
        rankLayerPrefab: {
            default: null,
            type: cc.Prefab
        },
    },

    onLoad: function() {
        cc.audioEngine.playMusic(this.audioBgm, true);
        UserData.loadData();
    },

    update: function(dt) {
        
    },

    //打开排行榜
    openRank: function() {
        this.node.getChildByName('FingerCtrl').active = false;
        let exitLayer = cc.instantiate(this.rankLayerPrefab);
        this.node.addChild(exitLayer);
    },

    //开始游戏
    startGame: function() {
        cc.director.loadScene('Game');
    },

    //打开商店
    openShop: function() {
        Common.showTip("textures/nomoney", this.node);
    },

    //打开帮助说明
    openHelp: function() {
        UserData.userInfo.score += 1;
        UserData.saveData();
    },

    onKeyBackClicked: function () {
        Common.switchNodeFingerCtrl(this.node);
        let exitLayer = cc.instantiate(this.exitLayerPrefab);
        Common.switchNodeFingerCtrl(exitLayer);
        this.node.addChild(exitLayer);
    },
});