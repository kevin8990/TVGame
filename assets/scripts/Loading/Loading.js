cc.Class({
    extends: cc.Component,

    properties: {
        speed: 1,
        horizontalBar: {
            type: cc.ProgressBar,
            default: null
        },
        tip: {
            type:cc.Sprite,
            default:null
        },
        tipList: {
            type:[cc.SpriteFrame],
            default:[]
        }
        
    },

    onLoad: function () {
        this.horizontalBar.progress = 0;
        var index = Math.ceil(Math.random() * this.tipList.length) - 1;
        this.tip.spriteFrame = this.tipList[index]; 
    },

    update: function (dt) {
        this._updateProgressBar(this.horizontalBar, dt);
    },
    
    _updateProgressBar: function(progressBar, dt) {
        var progress = progressBar.progress;
        if(progress < 1.0 ) {
            progress += dt * this.speed;
        } 
        else {
            cc.director.loadScene('Main');
        }
        progressBar.progress = progress;
    }
});
